apt-get update && apt-get upgrade -y
apt-get install -y cron wget nano git openssh-server rsync inotify-tools nodejs npm
wget -P /usr/local/bin/ https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions
chmod +x /usr/local/bin/install-php-extensions
install-php-extensions mysqli mcrypt bcmath curl intl json mbstring pdo_mysql readline session soap zip gd xsl sockets
composer global require mage2tv/magento-cache-clean
npm install -g grunt-cli
echo 'alias mage="sh /helper/mage"' >> ~/.bashrc
echo 'alias m2codestand="sh /helper/m2codestand"' >> ~/.bashrc