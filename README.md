# Dockar server for host your project with service nginx, mysql, elasticsearch

## Setup project with docker external volume

### Setup project in you local pc

- Clone this repository
- Copy your host machine `~/.ssh/*` files to `/projects/php/helper/ssh/` so it will use your host machine keys to connect with bitbucket or github.
- Start mysql and elasticsearch service.
- for mysql service first create this directory if not generate automatic `./mysql/data` and `./mysql/dump`.

```bash
cd mysql
docker-compose up -d
cd ../elasticsearch
docker-compose up -d
```

> Note: if you got error during start mysql container then first check for `data` and `dump` directory is exist or not and if directory is exist and again got same error then remove `:nocopy,delegated` from `.projects/mysql/docker-compose.yml` file for `./dump` volume and run this command for rebuilt docker container `docker-compose up -d --build`.

- Set volume `external: false` when you setup start project at first time, If volume is already exest then do not change.

`projects/docker-compose.yml`
```yml
volumes:
  projects-volume:
    name: projects-volume
    external: false
```

> Note: this volume is external which created at once when you setup the project. after `compose up` we set value for external is false because first time it cant find the volume so it will create it. Now, volume is created and now we use this volume in every container, now it is common volume for all the container. 

- Set volume `external: true` so every time you build the container it will use `projects-volume` as default volume.

`projects/docker-compose.yml`
```yml
volumes:
  projects-volume:
    name: projects-volume
    external: true
```

- Start container and done the setup.

```bash
bhavesh@Bhaveshs-iMac projects % pwd
/Users/bhavesh/Documents/projects
bhavesh@Bhaveshs-iMac projects % docker-compose up -d
```

now you can configure your projects with docker server.

## Add New project in our volume.

after start all containers: `nginx`, `phpfpm`, `mysql`, `elasticsearch`, `redis`, `rabbitmq` we are ready to start our project setup. This is one time process after start all container you no need to do anything else, docker will handel all stuf, it will automatic start when you start your computer and up all containers which was running when you quit docker. so project will start automaticaly after one time setup.

after start all services it look like this:

```bash 
bhavesh@Bhaveshs-iMac Documents % docker ps
CONTAINER ID   IMAGE                   COMMAND                  CREATED       STATUS       PORTS                                                                                                         NAMES
859f4c39f9d7   nginx:latest            "/docker-entrypoint.…"   7 days ago    Up 3 hours   0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp                                                                      nginx
aabc6f4e347f   php:7.4-fpm             "docker-php-entrypoi…"   7 days ago    Up 3 hours   0.0.0.0:9000->9000/tcp                                                                                        phpfpm74
d3b933502853   rabbitmq:3-management   "docker-entrypoint.s…"   2 weeks ago   Up 3 hours   4369/tcp, 5671/tcp, 0.0.0.0:5672->5672/tcp, 15671/tcp, 15691-15692/tcp, 25672/tcp, 0.0.0.0:15672->15672/tcp   rabbitmq-rabbitmq-1
717d31a316d4   redis                   "docker-entrypoint.s…"   2 weeks ago   Up 3 hours   0.0.0.0:6379->6379/tcp                                                                                        redis-redis-1
ba02b88b1a74   mysql                   "docker-entrypoint.s…"   5 weeks ago   Up 3 hours   0.0.0.0:3306->3306/tcp, 33060/tcp                                                                             mysql
df9c5969bc91   adminer                 "entrypoint.sh docke…"   5 weeks ago   Up 3 hours   0.0.0.0:8080->8080/tcp                                                                                        mysql-adminer-1
0ea16a6da58a   elasticsearch:7.9.1     "/tini -- /usr/local…"   5 weeks ago   Up 3 hours   0.0.0.0:9200->9200/tcp, 0.0.0.0:9300->9300/tcp                                                                elasticsearch-elasticsearch-1
```

> We need to famaliar with some docker commands. here `docker ps` list all running contaners, and `docker ps -a` list all container.

### ProjectX setup

We have create container for eache service. 

- Login to container `phpfpm74` using command `docker exec -it phpfpm74 bash` and create directory for projectx and move into it if not exist.

```bash
bhavesh@Bhaveshs-iMac projects % docker exec -it phpfpm74 bash
[phpfpm74 /var/www/html] $ cd /projects/         
[phpfpm74 /projects] $ mkdir projectx
[phpfpm74 /projects] $ cd projectx
```

> here `docker exec` command is use for run the command inside the container and with `-it` argument we get in to the container because `i` means `interactive` use for `STDIN` and `t` is `tty` use for login from host to container

> If you are using vscode then use [THIS VSCODE PLUGIN ](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) for docker.

> Syntax: docker exec {option} [container name/ identifire] {command}

- Clone project

```bash
[phpfpm74 /projects/projectx] $ ssh -T git@bitbucket.org
The authenticity of host 'bitbucket.org (104.192.141.1)' cant be established.
RSA key fingerprint is SHA256:zzXQOXSRBEiUtuE8AikJYKwbHaxvSc0ojez9YXaGp1A.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'bitbucket.org,104.192.141.1' (RSA) to the list of known hosts.
authenticated via ssh key.

You can use git to connect to Bitbucket. Shell access is disabled

[phpfpm74 /projects/projectx] $ git clone git@bitbucket.org:seepossible/projectx.git .
```

- Add `auth.json` file to project.

```bash
[phpfpm74 /projects/projectx] $ cp /helper/projectx/auth.json ./
```

> We have mount `/helper/` directory in `phpfpm74` container and we will use this directory for share files between the host and docker container.

- Run composer installer.

```bash
[phpfpm74 /projects/projectx] $ composer install --ignore-platform-reqs
```

### Server configuration and use.

#### Import database in to `mysql server`

> Note: we have mount `/dump/` directory in mysql container so, add database file in `projects/mysql/dump/` directory so we can access it inside the container.

- Login to `mysql` container and move into `/dump` directory.

```bash
bhavesh@Bhaveshs-iMac projects % docker exec -it mysql bash
root@ba02b88b1a74:/$ cd /dump/
root@ba02b88b1a74:/dump# ls
uat_projectx.sql

```

- login into mysql and create data base

> We have define default mysql user password in our docker compose file: `./projects/mysql/docker-compose.yml`. here we have use `rootroot` as password

> you can use adminer for create database [http://localhost:8080](http://localhost:8080) use host: `host.docker.internal` user: `root` and password: `rootroot`

```sql

root@ba02b88b1a74:/dump# mysql -u root -prootroot
mysql: [Warning] Using a password on the command line interface can be insecure.
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 78
Server version: 8.0.28 MySQL Community Server - GPL

Copyright (c) 2000, 2022, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> create database uat_projectx;
Query OK, 1 row affected (0.01 sec)

mysql> use uat_projectx;
Database changed
mysql> source uat_projectx.sql
```

or

```sql
root@ba02b88b1a74:/dump# mysql -u root -prootroot uat_projectx < uat_projectx.sql
```

> You can use adminer for login and edit mysql database using http://localhost:8080/

> use `mysql`, and `elasticsearch` host `host.docker.internal` for magento config and adminer login.

> use `projects/nginx/conf.d/` directory for `nginx` project configuration.

`projects/nginx/conf.d/projectx.conf`

```conf
server {

    server_name bhavesh.px.seepossible.link;
    index index.php;

    set $MAGE_ROOT /projects/projectx;
    set $MAGE_MODE production;

    # root /projects/projectx;

    access_log /var/log/nginx/projectx-access.log;
    error_log /var/log/nginx/projectx-error.log;

    include /projects/projectx/nginx.conf.sample;
    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass phpfpm74:9000;
        fastcgi_index index.php;
        fastcgi_buffers 16 16k; 
        fastcgi_buffer_size 32k;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }

    listen 80;
    listen [::]:80;
}
```

## configure redis and rabbitmq in projectx

first of all start the service `radis` and `rabbitmq` 

```bash
cd ./radis
docker-compose up -d
cd ./rabbitmq
docker-compose up -d
```

change the `app/etc/env.php` file.

```php
<?php
return [
    ...
    'cache' => [
        'frontend' => [
            'default' => [
                'id_prefix' => 'd81_',
                'backend' => 'Magento\\Framework\\Cache\\Backend\\Redis',
                'backend_options' => [
                    'server' => 'host.docker.internal',
                    'database' => '0',
                    'port' => '6379'
                ]
            ],
            'page_cache' => [
                'id_prefix' => 'd81_',
                'backend' => 'Magento\\Framework\\Cache\\Backend\\Redis',
                'backend_options' => [
                    'server' => 'host.docker.internal',
                    'port' => '6379',
                    'database' => '1',
                    'compress_data' => '0'
                ]
            ]
        ],
        'allow_parallel_generation' => false
    ],
    ...
    'queue' => [
        'amqp' => [
            'host' => 'host.docker.internal',
            'port' => '5672',
            'user' => 'admin',
            'password' => 'admin123',
            'virtualhost' => '/'
        ],
        'consumers_wait_for_messages' => 1
    ],
    ...
];

```

## Admin side configuration change

- Store > Setting > Configuration
- ELGENTOS > LargeConfigProducts
- Prewarm setting 
  - Absolute Path	: /projects/projectx
  - Redis Host : host.docker.internal
  - stay other as it is and save config.
- AMASTY EXTENSION > Elastic Search
- Connection
  - Elasticsearch Server Hostname : host.docker.internal
  - Enable Elasticsearch HTTP Auth : no
  - run Test Connection and save config.

## Some useful command for setting projectx

```bash
composer install --ignore-platform-reqs
chmod -R 777 var generated/ pub/static
cp app/etc/.env.php app/etc/env.php
cp app/etc/.config.php app/etc/config.php
```

# Thank you, All the best.