while inotifywait -r -e modify,create,delete,move /helper/sync; do
    rsync -avz --exclude {'/helper/sync/projectx/var/*', '/helper/sync/projectx/generated/code/*', '/helper/sync/projectx/pub/static/*'} /helper/sync/ /projects/
done